let mix = require('laravel-mix');

mix.options({
        processCssUrls: false,
        autoprefixer: {
            enabled: true,
            options: {
                overrideBrowserslist: ['last 2 versions', '> 1%'],
                cascade: true,
                grid: true,
            }
        },
    })
    .setPublicPath('/')
    .sass('src/scss/app.scss', 'assets/css/styles.css')
    .sass('src/scss/promo/main.scss', 'assets/css/promo.css')
    .js('src/js/app.js', 'assets/js/scripts.js')
    .js('src/js/promo.js', 'assets/js/promo.js')
    .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'assets/webfonts')
    .version();
