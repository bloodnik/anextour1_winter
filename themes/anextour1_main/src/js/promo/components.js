"use strict";
Vue.use(VueMaterial.default);

Vue.component("tour-select-popup", {
  data: () => ({}),
  methods: {
    openWindow() {},
  },
  template: `<button  class="button button_large">Получить подборку</button>`,
});

new Vue({
  el: "#tour-selection",

  computed: {
    formValid() {
      return {
        name: !!(this.form.name && this.form.name.length),
        phone: !!(this.form.phone && this.form.phone.length),
        agreement: !!this.agreement,
      };
    },
  },

  mounted() {
    this.$material.locale.firstDayOfAWeek = 1;
    this.$material.locale.dateFormat = "dd.MM.yyyy";

    this.$material.locale.days = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
    this.$material.locale.shortDays = ["Вс", "Пон", "Вт", "Ср", "Чт", "Пт", "Сб"];
    this.$material.locale.shorterDays = ["В", "П", "В", "С", "Ч", "П", "С"];
    this.$material.locale.months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
    this.$material.locale.shortMonths = ["Янв", "Фев", "Мар", "Апр", "Май", "Июнь", "Июнь", "Авг", "Сен", "Окт", "Ноя", "Дек"];
  },

  data() {
    let now = new Date();

    return {
      form: {
        country: document.querySelector("input[name=country]").value || "",
        date: now,
        nights: 9,
        adults: 2,
        childs: 0,
        stars: [3],
        meal: ["AL"],
        name: "",
        phone: "",
      },

      show: false,
      errorMsg: "",
      successMsg: "",
      agreement: true,
      loading: false,
    };
  },
  methods: {
    submitForm() {
      this.errorMsg = "";
      this.successMsg = "";
      this.loading = true;

      axios.get("/api/send.php", { params: this.form }).then((resp) => {
        const data = resp.data;
        if (data.hasError) {
          this.errorMsg = data.msg;
          this.loading = false;
        } else {
          this.successMsg = data.msg;
          ym(66187795, "reachGoal", "send_tour_form");
          console.log("send_tour_form");
          this.loading = false;
        }
      });
    },

    validate() {
      if (!this.formValid.name) {
        return false;
      }

      if (!this.formValid.phone) {
        return false;
      }

      if (!this.formValid.agreement) {
        return false;
      }

      return true;
    },

    showModal() {
      this.show = true;
    },

    hideModal() {
      this.show = false;
    },

    goToSearchPage() {
      location.href = "/tour-search/";
    },
  },
});
