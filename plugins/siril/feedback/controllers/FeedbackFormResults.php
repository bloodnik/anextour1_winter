<?php namespace Siril\Feedback\Controllers;

use Backend\Models\User;
use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use October\Rain\Support\Facades\Input;
use October\Rain\Support\Facades\Validator;
use Siril\Feedback\Models\FeedbackFormResult;

/**
 * Feedback Form Results Back-end Controller
 */
class FeedbackFormResults extends Controller {
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct() {
        parent::__construct();

        BackendMenu::setContext('Siril.Feedback', 'feedback', 'feedbackformresults');
    }


    private function getAdminsEmails() {
        $admins = User::whereHas('groups', function($query) {
            $query->where('code', 'notify-admins');
        })->get();

        $emails = [];
        foreach ($admins as $admin) {
            $emails[] = $admin->email;
        }

        return $emails;

    }

    /**
     * Отправка результата на почту и сохранение в БД
     *
     */
    public function send() {

        $validator = Validator::make(
            [
                'name'         => Input::get('name'),
                'phone'        => Input::get('phone'),
                'text_message' => Input::get('text_message'),
            ],
            [
                'name'         => ['required'],
                'phone'        => ['required'],
                'text_message' => ['required'],
            ],
            [
                'name.required'         => 'Введите ваше имя',
                'phone.required'        => 'Введите ваше телефон',
                'text_message.required' => 'Введите сообщение',
            ]
        );

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'success' => 'false',
                'errors'  => $validator->errors()->all(),
            ], 400);

        }

        $responseParams = Input::all();

        $this->sendMailNotify($responseParams);

        $result = FeedbackFormResult::create($responseParams);

        return response()->json([
            "success" => true,
            "result"  => $result
        ], 200);
    }

    // Отправляем почту
    private function sendMailNotify($responseParams) {
        Mail::send('feedbackTemplate', $responseParams, function(Message $message) {
            $emails = $this->getAdminsEmails();
            if ( ! empty($emails)) {
                foreach ($emails as $email) {
                    $message->to($email);
                }
            }

            $message->subject("Письмо директору - Pegast1");
        });
    }
}
