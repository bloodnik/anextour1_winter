<?php namespace Siril\Feedback\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateFeedbackFormResultsTable extends Migration {
    public function up() {
        Schema::table('siril_feedback_feedback_form_results', function(Blueprint $table) {
            $table->string('email')->default(null)->change();
        });
    }

    public function down() {
    }
}
