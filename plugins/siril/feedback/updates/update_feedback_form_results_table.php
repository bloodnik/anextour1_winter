<?php namespace Siril\Feedback\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateFeedbackFormResultsTable extends Migration
{
    public function up()
    {
        Schema::table('siril_feedback_feedback_form_results', function (Blueprint $table) {
            $table->renameColumn('message', 'text_message');
        });
    }

    public function down()
    {
        Schema::table('siril_feedback_feedback_form_results', function (Blueprint $table) {
            $table->renameColumn('text_message', 'message');
        });

    }
}
