<?php namespace Siril\Feedback\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFeedbackFormResultsTable extends Migration
{
    public function up()
    {
        Schema::create('siril_feedback_feedback_form_results', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email');
            $table->text('message')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('siril_feedback_feedback_form_results');
    }
}
