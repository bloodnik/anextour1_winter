<?php namespace Siril\PickUpTour\Models;

use Model;

/**
 * PickUpFormResult Model
 */
class PickUpFormResult extends Model {
    use \October\Rain\Database\Traits\Validation;

    protected $guarded = [
        '_method',
        '_token'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'siril_pickuptour_pick_up_form_results';


    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

}
