<?php namespace Siril\PickUpTour\Controllers;

use Backend\Models\User;
use BackendMenu;
use Backend\Classes\Controller;
use DateTime;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use October\Rain\Network\Http;
use October\Rain\Support\Facades\Input;
use October\Rain\Support\Facades\Validator;
use Siril\PickUpTour\Models\PickUpFormResult;

/**
 * Pick Up Form Results Back-end Controller
 */
class PickUpFormResults extends Controller {
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct() {
        parent::__construct();

        BackendMenu::setContext('Siril.PickUpTour', 'pickuptour', 'pickupformresults');
    }


    private function getAdminsEmails() {
        $admins = User::whereHas('groups', function($query) {
            $query->where('code', 'notify-admins');
        })->get();

        $emails = [];
        foreach ($admins as $admin) {
            $emails[] = $admin->email;
        }

        return $emails;

    }

    /**
     * Отправка результата на почту и сохранение в БД
     *
     */
    public function sendResult() {

        $validator = Validator::make(
            [
                'name'      => Input::get('name'),
                'phone'     => Input::get('phone'),
            ],
            [
                'name'      => ['required'],
                'phone'     => ['required'],
            ],
            [
                'name.required'      => 'Введите ваше имя',
                'phone.required'     => 'Введите ваше телефон',
            ]
        );

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'success' => 'false',
                'errors'  => $validator->errors()->all(),
            ], 400);

        }

        $responseParams = Input::all();

        $this->sendMailNotify($responseParams);
        $this->sendCRMNotify($responseParams);

        $result = PickUpFormResult::create($responseParams);

        return response()->json([
            "success" => true,
            "result"  => $result
        ]);
    }

    // Отправляем почту
    private function sendMailNotify($responseParams) {
        Mail::send('pickMeTourTemplate', $responseParams, function(Message $message) {
            $emails = $this->getAdminsEmails();
            if ( ! empty($emails)) {
                foreach ($emails as $email) {
                    $message->to($email);
                }
            }

            $message->subject("Заявка на подбор тура - Anextour1");
        });
    }

    // Отправляем уведомление в CRM
    private function sendCRMNotify($responseParams) {
        $api_key = 'vLsZl8Rr7wtH34E2F35MBur9enoum6F8mZTd1X9Yy5MNO79ZW74IbX5Tt5G70Zdm';
        $url     = 'https://golden-tours.moidokumenti.ru/api/add-lead';

        $params = array(
            'name'   => htmlspecialchars($responseParams["name"]),
            'phone'  => htmlspecialchars($responseParams["phone"]),
            'source' => 'Заявка с сайта Anextour1.ru',
            'fields' => array(
                array(
                    'name'   => 'Желаемая страна отдыха',
                    'values' => array($responseParams["country"])
                )
            )
        );

        $request = array(
            'params' => json_encode($params),
            'key'    => $api_key
        );

        Http::post($url, function(Http $http) use ($request) {
            $http->data($request);
            $http->timeout(600);
        });
    }
}
